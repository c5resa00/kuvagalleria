<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/custom.css'>
  
</head>
<body>
<br>
<br>
    
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand">Kuvagalleria</a>

  <span class="navbar-toggler-icon"></span>

<div class="collapse navbar-collapse" id=navbarCollapse">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
            <a class="nav-link" href="add.php">Lisää
                <span class="sr-only">(current)</span>
                <i class="for fa-camera"></i>
</a>
        </li>
    </ul>
</div>
</nav>
<div class="container">
    <div class="row">
    <div class="col">
   
<?php
$folder = 'uploads';
$handle = opendir($folder);
if($handle) {
    print"<div id='images'></div>";
    print"<div class='card-group'>";
    print"<div class='row'>";

while(false !== ($file = readdir($handle))) {


  
    $ext = substr( strrchr($file, '.'), 1);
    $file_extension = $ext;
    if(strtoupper($file_extension) === 'PNG' || strtoupper($file_extension)){
        $path = $folder . '/' . $file;
        
        $thumbs_path = $folder . '/thumbs/' . $file;
        
        // First get your image
$image = $thumbs_path;
$picture = base64_encode(file_get_contents($image));
echo '<img width="150" height="150" src="data:image/jpg;base64,'. $picture .'" />';

        
    
    }
}
} 
   ?>
    
        <div class="card p-2 col-lg-3 col-md-4 col-sm-6">
        <a data-fancybox class="fancybox"   href="<?php print $file; ?>">
            <img class="card-img-top" src="<?php print $file;?>">
            </a>
            <div class="card-body">
                <p class="card-text"><?php print $file; ?></p>
            

            </div>
    
        </div>
</div>
</div>

    </div>
</div>
</div>
<?php require_once 'inc/top.php'; ?>

</div>
</form>
<?php require_once 'inc/bottom.php'; ?>

</body>
</html>

