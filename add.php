<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='css/custom.css'>
  
</head>
<body>

<br>
<br>
    
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand">Kuvagalleria</a>

  <span class="navbar-toggler-icon"></span>

<div class="collapse navbar-collapse" id=navbarCollapse">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
            <a class="nav-link" href="add.php">Lisää
                <span class="sr-only">(current)</span>
                <i class="for fa-camera"></i>
</a>
        </li>
    </ul>
</div>
</nav>
<div class="container">
    <div class="row">
    <div class="col">

    </div>
</div>
</div>
<br>
<br>
<?php require_once 'inc/top.php'; ?>
<div class="form-group">
<form action="save.php" method="POST" enctype="multipart/form-data">
<label for="file">Tiedosto</label>
<input type="file" class="form-control" id="file" name="file">
<button class="btn btn-primary">Lataa</button>
</div>
</form>
<?php require_once 'inc/bottom.php'; ?>
</body>
</html>

